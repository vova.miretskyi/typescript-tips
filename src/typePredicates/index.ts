const sortOrderValues = ["asc", "desc"] as const;

type SortOrder = (typeof sortOrderValues)[number];

namespace SortOrder {
  export function tryParse(input: string): input is SortOrder {
    return (sortOrderValues as readonly string[]).includes(input);
  }
}
function search(query: string, sortOrder: SortOrder) {
  console.log("sortOrder: ", sortOrder);
}

function getSortOrder(sortOrder: SortOrder): string {
  return sortOrder;
}

const qOrder: string = getSortOrder("asc");

const order = SortOrder.tryParse(qOrder) ? qOrder : "asc";

search("foo", order);
